> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Quinn Nguyen

### Assignment 2 Requirements:
1. Backward-engineer (using Python)
2. The program should be organized with two modules (function.py and main.py)
3. Questions A2
4. Bitbucket repos link
  -----------------------------------------------
### Assignment 2 README.md file should include:
  - screenshot of Assigment 2 calculate_payroll application running
  - screenshot of Payroll No Overtime
  - screenshot of Payroll With Overtime
  -----------------------------------------------

### Assignment Screenshots: 
*Screenshot of payroll_calculator application running (Visual Studio Code)*
####               Payroll No Overtime
![A2 no overtime](img/noo.png)

####               Payroll With Overtime
![A2 overtime](img/over.png)

*Screenshot of payroll_calculator application running (IDLE)*
####               Payroll No Overtime
![A2 no overtime](img/nooi.png)

####               Payroll With Overtime
![A2 overtime](img/overi.png)



[A2 Bitbucket repos](https://bitbucket.org/qn18/lis4369/src/master/ "Bitbucket repos")







