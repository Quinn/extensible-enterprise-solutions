> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Quinn Nguyen

### Assignment 5  Requirements:

1. Requirements:
- a.Complete the following tutorial: Introduction_to_R_Setup_and_Tutorial
- b.Code and run lis4369_a5.R
- c.Be sure to include at least two plots in your README.md file.

2. Be sure to test your program using RStudio.
3. Questions
4. A5 Bitbucket repo links
   ---------------------------------------------------------
### Assignment 5 README.md file should include:
- Assignment requirements, as per A1.
- Screenshots of output from a5.R code .
   ---------------------------------------------------------
### Assignment 5 Introduction_to_R_Setup_and_Tutorial
- 1.What is R? (and, download)
- 2.What is RStudio? (and, download)
- *Carefully* go through the entirefollowing tutorial(up to and including p. 32):LEARN TO USE RYour hands-on guide: http://core0.staticworld.net/assets/2015/02/17/r4beginners.pdf

- Deliverables(see examplescreenshotbelow):
-   1.R Commands:save a file of all the R commands included in the tutorial.
-   2.R Console:save a screenshot of some of the R commands executed above(below requirement).
-   3.Graphs:save at least 5 separate image files displaying graph plotscreated from the tutorial.
-   4.RStudio:save one screenshot (similar to the one below), displaying the following 4 windows:
-       a.R source code(top-left corner)
-       b.Console(bottom-left corner)
-       c.Environment(or History), (top-right corner)
-       d.Plots(bottom-right corner)
   ---------------------------------------------------------
## Deliverables:
- 1. R Commands:
![A5 R command](a5code/practice.R)

- 2.R Console: screenshot of some of the R commands executed
![A5 pratice command 1](imge/b1.png)
![A5 pratice command 2](imge/b2.png)

- 3.Graphsfrom the tutorial
![A5 graph 1](imge/c1.png)
![A5 graph 2](imge/c2.png)
![A5 graph 3](imge/c3.png)
![A5 graph 4](imge/c4.png)
![A5 graph 5](imge/c5.png)
![A5 graph 6](imge/c6.png)
![A5 graph 7](imge/c7.png)

- 4. Screenshot of displaying 4 windows:
![A5 window 1](imge/d1.png)
    

### Assignment Screenshots:

1. 
*SCREEN SHOT OF A5 R commands executed* 
![A5 code 1](img/a1.png)
![A5 code 2](img/a2.png)
![A5 code 3](img/a3.png)
![A5 code 4](img/a4.png)
![A5 code 5](img/a5.png)
![A5 code 6](img/a6.png)
![A5 code 7](img/a7.png)
![A5 code 8](img/a8.png)
![A5 code 9](img/a9.png)
![A5 code 10](img/a10.png)


2.  
*SCREEN SHOT OF A5 plots*
![A5 graph 1](img/b1.png)
![A5 graph 2](img/b2.png)
![A5 graph 3](img/b3.png)
![A5 graph 4](img/b4.png)
![A5 graph 5](img/b5.png)


3. 
[A5 Bitbucket repos](https://bitbucket.org/qn18/lis4369/src/master/ "Bitbucket repos")







