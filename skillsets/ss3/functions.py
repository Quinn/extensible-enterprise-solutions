

def get_requirements():
    print("Miles Per Gallon")
    print("\nProgram Requirements:\n" 
        + "1. Convert MPG.\n"
        + "2. Must use float data type for user input and calculation.\n"
        + "3. Format and round conversion to two decimal places.")


def calculate_miles_per_gallon():
    #initialize variables
    miles = 0.0
    gallons = 0.0
    mpg = 0.0

    #get user data
print("\nUser Input:")
miles = float(input("Enter miles driven: "))
    #gallons of fuel used
gallons = float(input("Enter gallons of fuel used: "))

    #calculate miles per gallon
mpg = miles / gallons

print("\nOutput:")
print (format(miles,".2f") + " miles driven and " + format(gallons,".2f") + " gallons used = " + format(mpg,".2f") + " mpg.")