> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Quinn Nguyen

### Assignment 1 Requirements:
1. Distributed version control with git and bitbucket
2. Development Installations
3. Questions A1
4. Bitbucket repos link
  -----------------------------------------------
### Assignment 1 README.md file should include:
  - git commands with short descriptions
  - screenshot of A1 tip_calculator application running
  -----------------------------------------------
# Git commands w/short descriptions:

1. git init - create a new local repository
2. git status - List the files you've changed and those you still need to add or commit:
3. git add - add file
4. git commit -m - Commit changes to head (but not yet to the remote repository)
5. git push - Send changes to the master branch of your remote repository:
6. git pull - Fetch and merge changes on the remote server to your working directory
7. git remote -v - List all currently configured remote repositories

## Assignment Screenshots: 
*Screenshot of tip_calculator application running (IDLE)*
![A1-idle](img/idle.png)

*Screenshot of tip_calculator application running (Visual Studio Code)*
![A1 vs](img/vscode.png)


4. 
[A1 Bitbucket repos](https://bitbucket.org/qn18/lis4369/src/master/ "Bitbucket repos")







