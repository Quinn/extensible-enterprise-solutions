def get_requirements():
    print("Painting Estimator")
    print("\nProgram Requirements:\n" 
        +"1. Calculate home interior paint cost (w/o primer).\n"
        +"2. Must use float data types.\n"
        +"3. Must use SQFT_PER_GALLON constant (350).\n"
        +"4. Must use iteration structure (aka 'loop').\n"
        +"5. Format, right-align numbers, and round to two decimal places.\n"
        +"6. Create at least five functions that are called by the program:\n"
        +"\ta. main(): calls two other functions: get_requirements() and estimate_printing_cost()\n"
        +"\tb. get_requirements(): displays the program requirements.\n" 
        +"\tc. estimate_painting_cost(): calculates interior home painting, and calls print functions.\n"
        +"\td. print_painting_estimate(): displays painting costs.\n"
        +"\te. print_painting_percentage(): displays painting costs percentages.\n")

def estimate_painting_cost():
    while True:
        print("\nUser Input:")
        sqft = float(input("Enter total interior sq ft: "))
        price_paint = float(input("Enter price per gallon paint: "))
        hour_paint = float(input("Enter hourly painting rate per sq ft: "))

        sqft_gallon = 350

        gallonUse = (sqft / 350) * 1
        labor = (sqft * hour_paint)
        paint = (gallonUse * price_paint)
        total = (paint + labor)

        paintp = (paint / total)
        laborp = (labor / total )
        totalp = (total / total)
def print_painting_estimate():
        print("\nOutput:")
        print("{0:25} {1:>8}".format("Item:", "Amount"))       
        print("{0:25} {1:>8.2f}".format("Total Sq Ft:", sqft))   
        print("{0:25} {1:>8.2f}".format("Sq Ft per Gallon:", sqft_gallon))   
        print("{0:25} {1:>8.2f}".format("Number of Gallon:", gallonUse))   
        print("{0:25} ${1:>7.2f}".format("Paint per Gallon:", price_paint))   
        print("{0:25} ${1:>7.2f}".format("Labor per Sq Ft:", hour_paint))   
        print(" ")
        
def print_painting_percentage():
        print("{0:10} {1:>8} {2:>15}".format("Cost", "Amount","Percentage"))       
        print("{0:10} ${1:>8.2f} {2:>14.2%}".format("Paint: ", paint, paintp))
        print("{0:10} ${1:>8.2f} {2:>14.2%}".format("Labor: ", labor, laborp))
        print("{0:10} ${1:>8.2f} {2:>14.2%}".format("Total: ", total, totalp))

        again = str(input("\nEstimate another paint job? (y/n): "))
        if again == "y":
            continue
        else:
            print("\nThank you for using our Painting Estimator!")
            print("Please see our web site: http://www.mysite.com")
            break
       
