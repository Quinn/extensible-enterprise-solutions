> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Quinn Nguyen

### Assignment 3 Requirements:
1. Backward-engineer (using Python)
2. The program should be organized with two modules (function.py and main.py)
    a. functions.pymodulecontains the following functions:
    i.  get_requirements()
    ii. estimate_painting_cost()
    iii.print_painting_estimate()
    iv. print_painting_percentage()
3. Questions for A3
4. Be sure to test program using both IDLEand Visual Studio Code.
5. Bitbucket repos link
  -----------------------------------------------
### Assignment 3 README.md file should include:
  - screenshot of Assigment 3 painting estimator application running
  - screenshot of A3 runnint in IDLE
  - screenshot of A3 running in VSCode
  -----------------------------------------------

### Assignment Screenshots: 
*Screenshot of Painting Estimator application running (Visual Studio Code)*
####               Visual Studio Code
![A3 VS](img/vsc.png)



*Screenshot of Painting Estimator application running (IDLE)*
####               IDLE
![A3 IDLE](img/idle.png)




[A3 Bitbucket repos](https://bitbucket.org/qn18/lis4369/src/master/ "Bitbucket repos")







