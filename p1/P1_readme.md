> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 

## Quinn Nguyen

### Project 1 Requirements:

1. Requirements: a. Code and run demo.py. (Note: be sure necessary packages are installed!) b. Then use it to backward-engineer the screenshots below it. c. Installing Python packages pip helper videos
2. Screenshot of data analysis on IDLE and Visual Studio Code
3. Questions Chapter 7 and 8.
4. P1 repos link
   -------------------------------------------------------------------
### Project 1 README.md file should include:
  - screenshot of P1 running in IDLE
  - screenshot of P1 running in VSCode
  - screenshot of P1 graph
   -------------------------------------------------------------------
### Assignment Screenshots:
1. 
*SCREEN SHOT OF P1_data_analysis in IDLE* 
![P1 IDLE 1](img/id1.png)
![P1 IDLE 2](img/id2.png)
![P1 IDLE 3](img/id3.png)

2.  
*SCREEN SHOT OF P1_data_analysis in Visual Studio Code*
![P1 Visual Studio 1](img/vs1.png)
![P1 Visual Studio 2](img/vs2.png)

3. 
*SCREEN SHOT OF P1_graph* 
![P1 graph](img/gr.png)

4. 
[P1 Bitbucket repos](https://bitbucket.org/qn18/lis4369/src/master/ "Bitbucket repos")







