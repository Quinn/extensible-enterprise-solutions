> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4369

## Quinn Nguyen

### MAIN README:

*Course Work Links:*

1. [A1 README.md](a1/A1_readme.md "My A1 README.md file")
    - Tip Calculator with Python
    - Install Python, R, R Studio, Visual Studio Code

2. [A2 README.md](a2/A2_readme.md "My A2 README.md file")
    - Payroll Calculator with Python
    - Payroll Calculator No Overtime
    - Payroll Calculator With Overtime

3. [A3 README.md](a3/A3_readme.md "My A3 README.md file")
    - Painting Estimator with Python
    - Painting Estimator running both Visual Studio Code and IDLE.
    - Review how to write Python
    
4. [A4 README.md](a4/A4_readme.md "My A4 README.md file")
    - Run demo.py
    - Create A4_data_analysis_2 by backward-engineering  
    - Why is the graph line split?
    - Be sure to test your program using both IDLE and Visual Studio Code.

5. [A5 README.md](a5/A5_readme.md "My A5 README.md file")
    - Run lis4369_A5.R
    - Complete tutorial: Introduce to R Setup and Tutorial
    - At least two plots in A5_Readme.md
    - Test code with RStudio

6. [P1 README.md](p1/P1_readme.md "My P1 README.md file")
    - Install Python packages pip
    - Run demo.py
    - Create p1_data_analysis_1 by backward-engineering the screenshots below
    - Provide screenshots running in IDLE and Visual Studio Code

7. [P2 README.md](p2/P2_readme.md "My P2 README.md file")
    - a. Use assignment 5 screenshots and references
    - b.Backward-engineer the lis4369_p2_requirements.txtfile
    - c.Be sure to include at least two plotsin your README.md file.
8. [SS README.md](skillsets/SS_readme.md "My Skillsets README.md file")
