> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Quinn Nguyen

### Assignment 4  Requirements:

1. Requirements:
- Code and run demo.py. (Note: be sure necessary packages are installed!)
- Then use it to backward-engineer the screenshots below it.
- When displaying the required graph (see code below), answer the following question: Why is the graph line split?
2. Be sure to test your program using both IDLE and Visual Studio Code
3. Questions
4. A4 Bitbucket repo links
   ---------------------------------------------------------
### Assignment 4 README.md file should include:
  - screenshot of A4 runnint in IDLE
  - screenshot of A4 running in VSCode
  - screenshot of A4 graph

### Assignment Screenshots:
1. 
*SCREEN SHOT OF A4_data_analysis in IDLE* 
![A4 IDLE 1](img/id1.png)
![A4 IDLE 2](img/id2.png)
![A4 IDLE 3](img/id3.png)
![A4 IDLE 4](img/id4.png)
![A4 IDLE 5](img/id5.png)
![A4 IDLE 6](img/id6.png)
![A4 IDLE 7](img/id7.png)
![A4 IDLE 8](img/id8.png)
![A4 IDLE 9](img/id9.png)
![A4 IDLE 10](img/id10.png)
![A4 IDLE 11](img/id11.png)

2.  
*SCREEN SHOT OF A4_data_analysis in Visual Studio Code*
![A4 Visual Studio 1](img/vs1.png)
![A4 Visual Studio 2](img/vs2.png)
![A4 Visual Studio 3](img/vs3.png)
![A4 Visual Studio 4](img/vs4.png)
![A4 Visual Studio 5](img/vs5.png)
![A4 Visual Studio 6](img/vs6.png)
![A4 Visual Studio 7](img/vs7.png)
![A4 Visual Studio 8](img/vs8.png)

3. 
*SCREEN SHOT OF A4_graph* 
![A4 graph](img/gr.png)

4. 
[A4 Bitbucket repos](https://bitbucket.org/qn18/lis4369/src/master/ "Bitbucket repos")







