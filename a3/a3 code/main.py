import functions as f

def main():
    f.get_requirements()
    check = "y"
    while check.lower() == "y":
        f.estimate_painting_cost()
        print()
        check = input("Estimate another paint job?(y/n): ")
        print()

    print("\nThank you for using our Painting Estimator!")
    print("Please see our web site: http://www.mysite.com")

if__name__== "__main__":
    main()
