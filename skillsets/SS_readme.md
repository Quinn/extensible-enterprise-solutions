> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Quinn Nguyen

### Skillsets

1. [S1 README.md](ss1/S1_readme.md "My SS1 README.md file")


2. [S2 README.md](ss2/S2_readme.md "My SS2 README.md file")









