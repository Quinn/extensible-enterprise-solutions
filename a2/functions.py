def get_requirements():
    print("Payroll Calculator")
    print("\nProgram Requirements:\n" 
        + "1. Must use float data type for user input.\n"
        + "2. Overtime rate: 1.5 times hourly rate (hours over 40).\n"
        + "3. Holiday rate: 2.0 times hourly rate (all holiday hours).\n"
        + "4. Must format currency with dollar sign, and round to two decimal places.\n"
        + "5. Create at least three functions that are called by the program:\n"
        + "\ta. main(): callas at least two other functions.\n"
        + "\tb. get_requirement(): displays the program requirements.\n"
        + "\tc. calculate_payroll(): calculates an individual one-week.")

def calculate_payroll():
    BASE_HOUR = 40
    OT_RATE = 1.5
    HOLIDAY_RATE = 2.0

    print("\nUser Input:")
    hours = float(input("Enter hours worked: "))
    hldHour = float(input("Enter holiday hours: "))
    pay = float(input("Enter hourly pay rate: "))

    basePay = (BASE_HOUR * pay)
    overtime = (hours - BASE_HOUR)

    if hours > BASE_HOUR:
        overtimePay = (overtime * pay * OT_RATE)

    hld = hldHour * pay * HOLIDAY_RATE
    total = (basePay + overtimePay + hld)

    print("\nOutput:")
    print("{0:12} ${1:,.2f}".format("Base:", basePay))
    print("{0:12} ${1:,.2f}".format("Overtime:", overtimePay))
    print("{0:12} ${1:,.2f}".format("Holiday:",hld))
    print("{0:12} ${1:,.2f}".format("Gross:",total))
