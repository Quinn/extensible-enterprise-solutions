> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369

## Quinn Nguyen

### Project 2  Requirements:

1. Requirements:
- a.Use assignment 5 screenshots and references
- b.Backward-engineer the lis4369_p2_requirements.txtfile
- c.Be sure to include at least two plotsin your README.md file.

2. Be sure to test your program using RStudio.
3. Questions
4. P2 Bitbucket repo links
   ---------------------------------------------------------
### Project 2 README.md file should include:
- Assignment requirements, as per A1.
- Screenshots of output from p2.R code .
   ---------------------------------------------------------
## Screenshots:
- 1. R Code P2:
![P2 R code](img/c1.png)

- 2.R Console: screenshot of some of the R commands executed
![P2 plot 1](img/p1.png)
![P2 plot 2](img/p2.png)

3. 
[P2 folder](https://bitbucket.org/qn18/lis4369/src/master/p2/ "P2 link")







x